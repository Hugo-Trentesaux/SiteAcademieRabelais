Site Django concu pour l'académie Rabelais
===================================
Techniquement
-----------
### Auteur
Hugo Trentesaux
[trentesauxhugo@gmail.com](mailto:trentesauxhugo@gmail.com)

### Un mot sur Django
[Django](https://www.djangoproject.com/start/overview/) est un framework python très puissant et agréable à utiliser.
Il est de plus très souple, ce qui permet de développer des fonctionnalités très facilement.

### Serveur de test
Un serveur de production expérimental est disponible [ici](http://51.254.103.68:8000/). Il utilise apache et son module mod_wgsi.

En pratique
-----------
Le site doit proposer une [interface d'administration](http://51.254.103.68:8000/admin/) simple pour ajouter des membres, des bistrots, et des activités.
Des fonctionnalités supplémentaires pourront être développées au fur et à mesure si nécessaire.