from django.contrib import admin

from .models import Activity, Membre, Bistrot

admin.site.register(Activity)
admin.site.register(Membre)
admin.site.register(Bistrot)
