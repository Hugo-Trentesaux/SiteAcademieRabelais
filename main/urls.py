from django.conf.urls import url

from . import views

app_name = 'main'
urlpatterns = [
	url(r'^$', views.index, name='accueil'),
	url(r'^histoire/$', views.histoire, name='histoire'),
	url(r'^contact/$', views.contact, name='contact'),
	
	url(r'^membres/$', views.Membres.as_view(), name='membres'),
	url(r'^membre/(?P<pk>[0-9]+)$', views.MembreDetail.as_view(), name='membre'),
	
	url(r'^activités/$', views.Activities.as_view(), name='activités'),
	url(r'^activité/(?P<pk>[0-9]+)$', views.ActivityDetail.as_view(), name='activité'),
	
	url(r'^bistrots/$', views.Bistrots.as_view(), name='bistrots'),
	url(r'^bistrot/(?P<pk>[0-9]+)$', views.BistrotDetail.as_view(), name='bistrot'),
]
