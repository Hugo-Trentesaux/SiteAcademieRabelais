from django.db import models

class Activity(models.Model):
	titre = models.CharField(max_length=128)
	description = models.TextField()
	date = models.DateTimeField()
	duration = models.DurationField(blank=True, null=True)
	lieu = models.CharField(max_length=128, blank=True, null=True)
	def __str__(self):
		return self.titre
		
class Membre(models.Model):
	nom = models.CharField(max_length=64)
	prenom = models.CharField(max_length=64)
	date_naissance = models.DateField(blank=True, null=True)
	email = models.EmailField(blank=True, null=True)
	adresse = models.CharField(max_length=128, blank=True, null=True)
	def __str__(self):
		return self.prenom + ' ' + self.nom
		
class Bistrot(models.Model):
	nom = models.CharField(max_length=64)
	adresse = models.CharField(max_length=128)
	contact = models.CharField(max_length=128, blank=True, null=True)
	description = models.TextField()
	def __str__(self):
		return self.nom
