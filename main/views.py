from django.shortcuts import render
from django.views import generic

from .models import Activity, Membre, Bistrot

def index(request):
	activities = Activity.objects.order_by('-date')[:3]
	context = {'activities': activities}
	return render(request, 'main/index.html', context)
	
def histoire(request):
	return render(request, 'main/histoire.html')
	
def contact(request):
	return render(request, 'main/contact.html')
	
class Activities(generic.ListView):
	model = Activity
	template_name = 'main/activités.html'
	context_object_name = 'activities'
	def get_queryset(self):
		return Activity.objects.order_by('-date')

class ActivityDetail(generic.DetailView):
	model = Activity
	template_name = 'main/activité.html'

class Membres(generic.ListView):
	model = Membre
	template_name = 'main/membres.html'
	context_object_name = 'membres'
	def get_queryset(self):
		return Membre.objects.order_by('-date_naissance')

class MembreDetail(generic.DetailView):
	model = Membre
	template_name = 'main/membre.html'

class Bistrots(generic.ListView):
	model = Bistrot
	template_name = 'main/bistrots.html'
	context_object_name = 'bistrots'
	def get_queryset(self):
		return Bistrot.objects.order_by('nom')

class BistrotDetail(generic.DetailView):
	model = Bistrot
	template_name = 'main/bistrot.html'

