/* code javascript utilisé pour faire les animations
pour l'instant, ajoute juste un cadre de couleur autour des éléments correspondants au menu */


// box-shadow: h-pos v-pos (blur) (spread) (color) (inset);

function selectionner(classe) {
    var x = document.getElementsByClassName(classe);
    var i;
    for (i = 0; i < x.length; i++) {
	     if (classe=="h") x[i].style.boxShadow = "0 0 2px 0 rgba(4, 146, 210, 0.3) inset, 0 0 2px 0 rgba(0, 0, 0, 0.3)";
	else if (classe=="g") x[i].style.boxShadow = "0 0 2px 0 rgba(197, 17, 46, 0.3) inset, 0 0 2px 0 rgba(197, 17, 46, 0.3)";
	else if (classe=="a") x[i].style.boxShadow = "0 0 2px 0 rgba(0, 104, 56, 0.3) inset, 0 0 2px 0 rgba(0, 104, 56, 0.3)";
    }
}
function deselectionner(classe) {
    var x = document.getElementsByClassName(classe);
    var i;
    for (i = 0; i < x.length; i++) {
        x[i].style.boxShadow = "none";
    }
}
